# Today
* Data analysis on recorded .csv files
* Increase clock freq of the SoC to see if the recording can be done faster. Also speed up algorithm?
* LiteX documentation (note that different clock signals driving is impossible in a pure migen sim)
* Tests for the monitor component
* Continue simplified/refactored monitor recorder/receiver


# Notes
* _Modes_: 
    * Direct: both recorder and receiver are on the same SoC and can directly communicate per Signal
    * Split: One device records and sends data, the other receives and prints on serial

* Recorder/Sender tasks:
    * Put bus accesses in queue when an ack is given
    * (If selectByAck is given, shrink the signals to the minimum required number)
    * Concat all data from the queue into one signal
    * Gearbox that signal out to either 32 bit words or 1 bit
    * Write data into the C header files

 * Receiver tasks:
    * Has control register CSR that is controlled by the cpu
    * Gearbox the signal into 32 bits again if needed
    * Gate to DMA to prevent the "first-element-dropped" bug
    * DMA that writes to memory

* Firmware tasks:
    * Firmware gets placed on the device containing the receiver component
    * Implements a ISR that is called when the memory is half full, and prints the data from the memory out


* Usage of pins / IO for the monitor component:
    * Data Pin
    * Ready Pin
    * Valid Pin
    * (Queue Lock Control Pin) Likely not needed because this only exists to prevent printf from interfering with the usual boot sequence anyways