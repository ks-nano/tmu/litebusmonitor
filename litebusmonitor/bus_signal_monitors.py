from migen import *
from migen.fhdl.structure import _Operator, _Statement, _Value, DUID
from litex.soc.interconnect import *
from litex.soc.interconnect.csr import *
from litex.soc.interconnect.stream import *
from litex.soc.integration.soc import *
from litex.soc.integration.doc import ModuleDoc

from litebusmonitor.modules import ParityBitAdder

from typing import Dict, List, Tuple, Union
import sys

from litebusmonitor import lbm_misc
from litebusmonitor.lbm_misc import MigenValue, MigenValueList
from litebusmonitor.modules import MonitorDetector

from litex.soc.interconnect.axi import *



class SignalMonitor(Module, ModuleDoc):
    """
    This unit can monitor given signals and, when a trigger occurs, write the current values of those signals into a queue.
    It is strongly encouraged to give names to the signal monitors for easier debugging messages!
    
    You can either pass a list of signals, then the queue layout will have data lines with the names of sig+index (sig0, sig1, sig2, ....) or a dictionary, that maps the signals to string-names.

    mode can either be "sync" or "comb". This determines if the trigger has to be active in sync or comb mode in order for the queue to record the signals

    If mode is sync, you can set trigger_mode:
    true: Record values when the trigger is 1 (BINARY TRIGGER)
    changeRising: Record values when trigger goes from 0 to 1 (BINARY TRIGGER)
    changeFalling: Record values when trigger goes from 1 to 0 (BINARY TRIGGER)
    change: Record values when the trigger changes value (NON BINARY TRIGGER) (does NOT have to be binary. E.g. When address=0xA becomes address=0x9E)
    
    lockWhenFull: If the queue should be locked when it is full, or instead just receive and output new values
    """

    def __init__(self, trigger:Union[Signal, _Statement], signals:Union[List[Signal], Dict[str, Signal]], entries:int, name="sig_mon", mode="sync", 
        trigger_mode="changeRising", add_bus_idle_cycle_counter=True, add_trigger_counter=True, add_recorder_queue_level_counter=True, add_interrupt_counter=True,
        with_detector=True, detector_violation_led=None, detector_condition_list=None) -> None:
        if mode not in ["sync", "comb"]:
            lbm_misc.say(f"[SignalMonitor {name}]: Mode {mode} is not a valid mode. Trigger can be active either synchronously or in an async/comb fashion (use mode=sync or mode=comb)", error=True)
            sys.exit()

        # Dictionary that assigns names to all signals that the queue outputs (may be recorded from the bus or be any arbitrary signal defined by the programmer)
        self.sigdict : Dict[str, Signal] = signals if type(signals) is dict else dict(zip([f"sig{i}" for i in range(len(signals))], signals))    

        # Signal declarations

        self.lost_signals_counter = Signal(10)

        if add_trigger_counter:
            tc = Signal(5)
            assert "tc" not in self.sigdict.keys()
            self.sigdict["tc"] = tc

        if add_bus_idle_cycle_counter:
            csla = Signal(8)
            _csla = Signal(8)
            assert "csla" not in self.sigdict.keys()
            self.sigdict["csla"] = csla
        
        if add_recorder_queue_level_counter:
            fllen = int(log2(entries))
            fifolevel = Signal(fllen)
            assert "fifolevel" not in self.sigdict.keys()
            self.sigdict["fifolevel"] = fifolevel
            assert "fifolevel" in self.sigdict.keys()
        
        if add_interrupt_counter:
            interrupt_count = Signal(3, name_override="interrupt_count")
            assert"interrupt_count" not in self.sigdict.keys()
            self.sigdict["interrupt_count"] = interrupt_count


        # Recorder fifo
        self.recorder_fifo_layout = recorder_fifo_layout = [(sigName, len(self.sigdict[sigName])) for sigName in self.sigdict.keys()]
        lbm_misc.say(f"[SIGNAL MONITOR] Setting up SignalMonitor [Name: {name}, Mode: {trigger_mode}, Entry Length: {sum([l for _,l in recorder_fifo_layout])} bits, FIFO-Depth: {entries}]")

        # Can be used to block input from entering the queue
        self.lock_fifo = Signal()

        self.recorder_fifo = recorder_fifo = SyncFIFO(recorder_fifo_layout, depth=entries)
        self.submodules += self.recorder_fifo
        self.source = recorder_fifo.source


        # Signal behaviour
        if add_trigger_counter:
            self.sync += [
                If(recorder_fifo.sink.valid,
                    If(tc == 2**tc.nbits-1,
                        tc.eq(0)
                    ).Else(
                        tc.eq(tc + 1)
                    )
                )
            ]

        if add_bus_idle_cycle_counter:
            self.sync += [
                If(recorder_fifo.sink.valid,
                    _csla.eq(0),
                    
                ).Else(
                    _csla.eq(_csla + 1)
                )
            ]
            self.comb += If(recorder_fifo.sink.valid, csla.eq(_csla)).Else(csla.eq(0))

        if add_recorder_queue_level_counter:
            self.comb += fifolevel.eq(recorder_fifo.level)


        if add_interrupt_counter:
            _lastFifoLock = Signal()
            self.sync += [
                _lastFifoLock.eq(self.lock_fifo),
                If(self.lock_fifo & ~_lastFifoLock,
                    interrupt_count.eq(interrupt_count + 1)
                )
            ]

        # Add detector if needed
        if with_detector:
            self.submodules.detector = MonitorDetector(self.sigdict, recorder_fifo.sink.valid, detector_condition_list, violation_detected_led=detector_violation_led)



        # Connect signals to queue sink
        for sigName in self.sigdict.keys():
            self.comb += recorder_fifo.sink.__getattr__(sigName).eq(self.sigdict[sigName])

        # Fill the queue on trigger
        if type(trigger) in [Signal, _Value, _Statement, _Operator] or issubclass(type(trigger), _Value):
            if mode == "sync":
                lastTriggerValue = Signal(len(trigger))
                if trigger_mode == "true":
                    self.comb += recorder_fifo.sink.valid.eq(trigger & ~self.lock_fifo)
                elif trigger_mode == "changeRising":
                    self.comb += recorder_fifo.sink.valid.eq(trigger & ~lastTriggerValue & ~self.lock_fifo)
                elif trigger_mode == "changeFalling":
                    self.comb += recorder_fifo.sink.valid.eq(~trigger & lastTriggerValue & ~self.lock_fifo)
                elif trigger_mode == "change":
                    self.comb += recorder_fifo.sink.valid.eq(trigger != lastTriggerValue & ~self.lock_fifo)
                else:
                    lbm_misc.say(f"[SignalMonitor {name}]: Invalid mode for triggering. Use trigger_mode in true, changeRising, changeFalling or change")
                
                self.sync += [
                    lastTriggerValue.eq(trigger),
                    If(self.recorder_fifo.sink.valid & ~self.recorder_fifo.sink.ready & ~self.lock_fifo,
                        self.lost_signals_counter.eq(self.lost_signals_counter + 1)
                    )
                ]
            
            elif mode == "comb":
                self.comb += recorder_fifo.sink.valid.eq(trigger & ~self.lock_fifo)

        else:
            lbm_misc.say(f"[SignalMonitor {name}]: {trigger} is none of the following: Migen Signal, _Value, _Statement, _Operator", error=True)
            sys.exit()


class BusMasterMonitor(Module, ModuleDoc):
    """
        This component monitors master interfaces of a bus. The trigger decides when the access should be recorded:
        strobe
        ack
        err
        transmissionRecognized (ack or err)
        permanent (every cycle)

        Additionally master_interface can be a single interface or a list of interfaces. If it is a list, the triggers get OR'd and 
        all signals of all masters get recorded (instead of just one master).

        So to monitor a single master on successful transmissions: BusMasterMonitor(master_interface1, trigger="ack")
        To monitor all interfaces of the bus when successful: BusMasterMonitor(list(self.bus.masters.values()), trigger="ack") (this would record anytime one of the masters
        received an ack. Which one (or both) got it can later be parsed by reading the records)

        select_by_ack determines if all signals get put into the output queue or only the ones of the master that fired

        To make usage of the data just use self.source (or monitor.source in another module) and use it according to whether you selected queue or gearbox as output.
        """
    def __init__(self, master_interface : Union[wishbone.Interface, List[wishbone.Interface]], trigger : str = "transmissionRecognized", entries = 128, dma_bus:SoCBusHandler=None, select_by_ack=True,
        add_bus_idle_cycle_counter=True, add_trigger_counter=True, add_recorder_queue_level_counter=True, add_interrupt_counter=True,
        with_detector=True, detector_violation_led=None, detector_condition_list=None) -> None:

        # Helpers
        OR = lambda x,y: _Operator("|", [x,y])
        self.dma_bus = dma_bus
        self.select_by_ack = select_by_ack

        # Prepare interfaces 
        master_interfaces = [("0", master_interface)] if not type(master_interface) == list else [(str(i), master_interface[i]) for i in range(len(master_interface))]
        self.master_interfaces = master_interfaces = self.get_converted_wb_interfaces(master_interfaces) # Convert AXI(Lite) to WB when necessary

        # Hardcoded signals that every master has and are supervised on every master
        data : Dict[str, Signal] = {}
        for index, mint in master_interfaces:
            data.update({
                index+"adr": mint.adr,
                index+"we": mint.we,
                index+"sel": mint.sel,
                index+"ack": mint.ack,
                index+"err": mint.err,
            })
        
        lbm_misc.say("[BUS MASTER MONITOR] Setting up BusMasterMonitor. Targets:")
        self._neat_print_layout([(index, mint.name) for index, mint in master_interfaces], "Index", "Interface")

        # Define the Signal Monitor
        monitor_trigger = {
            "strobe": reduce(OR, [Cat(mint.adr, Replicate(mint.stb, mint.adr_width)) for _, mint in master_interfaces]),
            "ack": reduce(OR, [mint.ack for _, mint in master_interfaces]),
            "err": reduce(OR, [mint.err for _, mint in master_interfaces]),
            "transmissionRecognized": reduce(OR, [mint.ack | mint.err for _, mint in master_interfaces]),
            "permanent": ClockSignal()
        }
        monitor_trigger_mode = {
            "strobe": "change",
            "ack": "changeRising",
            "err": "changeRising",
            "transmissionRecognized": "changeRising",
            "permanent": "true"
        }

        self.mon = SignalMonitor(
            trigger=monitor_trigger[trigger],
            signals=data,
            entries=entries,
            name=trigger+"Monitor",
            trigger_mode=monitor_trigger_mode[trigger],
            add_bus_idle_cycle_counter=add_bus_idle_cycle_counter,
            add_trigger_counter=add_trigger_counter,
            add_recorder_queue_level_counter=add_recorder_queue_level_counter,
            add_interrupt_counter=add_interrupt_counter,
            with_detector=with_detector,
            detector_violation_led=detector_violation_led,
            detector_condition_list=detector_condition_list
        )
        self.submodules += self.mon


        # For debugging #
        lbm_misc.say("[BUS MASTER MONITOR] Unoptimized Output queue layout is: ", error=True)
        self._neat_print_layout([(str(k),len(v)) for k,v in data.items()], "Signalname", "Bit-Length", error=True)
        ##################

        temp_source = None

        if not select_by_ack:
            # Layout of output data. Sends the cycle when the data was received with the data
            self.layout = layout = [(k, len(data[k])) for k in data.keys()]
            temp_source = self.mon
        else:
            # Select which masters data gets saved, depending on which master ack'd. Might save a lot of memory
            # First all masters ack signals get concatted, master 0 being the LSB and master n the MSB
            # There is a case dict that assigns the queue to the masters signals:
            # When the concatted_acks was 0100 then Master 3 (third bit) acked, meaning the signal was 4, and the queues adr gets set to 3adr
            lbm_misc.say("[BUS MASTER MONITOR] Selecting by ack-signals; recording only one set of master signals at a time!")
            adrLen = max([len(data[k]) for k in data.keys() if k[-3:] == "adr"])
            selLen = max([len(data[k]) for k in data.keys() if k[-3:] == "sel"])
            weLen = max([len(data[k]) for k in data.keys() if k[-2:] == "we"])
            ackLen = max([len(data[k]) for k in data.keys() if k[-3:] == "ack"])
            errLen = max([len(data[k]) for k in data.keys() if k[-3:] == "err"])

            # Signal that concats all acks to later make the comparison possible of which signal was 1
            self.cca = concatted_acks = Signal(len(master_interfaces))
            self.comb += concatted_acks.eq(Cat(*[self.mon.source.__getattr__(str(i) + "ack") for i in range(len(master_interfaces))]))
            
            # Which master is firing
            acknowledged_master = Signal(ceil(log2(len(master_interfaces))), name_override="acknowledged_master")

            # Create optimized queue
            # First bool specifies if the value differs from master to master and whether it is included in the fifo source payload
            self.active_master_signal_list = [
                (False, acknowledged_master, acknowledged_master.nbits),
                (True, "adr", adrLen),
                (True, "we", weLen),
                (True, "sel", selLen),
                (True, "ack", ackLen),
                (True, "err", errLen)
            ]


            if add_bus_idle_cycle_counter:
                self.active_master_signal_list.append((False, "csla", self.mon.source.csla.nbits))
            
            if add_recorder_queue_level_counter:
                self.active_master_signal_list.append((False, "fifolevel", self.mon.source.fifolevel.nbits))
            
            if add_trigger_counter:
                self.active_master_signal_list.append((False, "tc", self.mon.source.tc.nbits))
            
            if add_interrupt_counter:
                self.active_master_signal_list.append((False, "interrupt_count", self.mon.source.interrupt_count.nbits))


            # Sanity Error Check
            for differs_by_master, name, _ in self.active_master_signal_list:
                assert not (differs_by_master and type(name) != str), f"{name} must be a string when it should be switched by master!"
                assert not (not differs_by_master and not (type(name) == str and name in self.mon.source.payload.__dir__()) and type(name) not in MigenValueList), f"{name} is neither present in the SignalMonitor queue nor is it a direct MigenValue. Maybe a typo or missing defintion?"

                if differs_by_master:
                    for i in range(len(master_interfaces)):
                        assert not (str(i) + name not in self.mon.source.payload.__dir__()), f"{str(i) + name} is not in the SignalMonitor FIFO source record!"

            # Queue and layout
            self.layout = layout = [((sig.name_override if type(sig) in MigenValueList else sig), length) for _,sig,length in self.active_master_signal_list]

            self.active_master_fifo = active_master_fifo = SyncFIFO(layout, 512) # That queue is way smaller than the first monitor queue, so we buffer here!
            self.submodules += active_master_fifo

            # Connect optimized queue to signal monitor queue
            self.comb += [
                active_master_fifo.sink.valid.eq(self.mon.source.valid),
                self.mon.source.ready.eq(active_master_fifo.sink.ready),
            ]

            # Multiplex queue signals from SignalMonitor into optimizedQueue depening on which master is active
            self.case_dict = {"default": []}
            for i in range(len(master_interfaces)):
                self.case_dict[2**i] = []
            self.connect_signal_monitor_to_optimized_fifo(self.active_master_signal_list)
            self.comb += Case(concatted_acks, self.case_dict)

            temp_source = active_master_fifo
    
    
        parity_adder = ParityBitAdder(self.layout, temp_source)
        self.submodules += parity_adder

        self.source = parity_adder.source
        self.layout = parity_adder.out_layout


        lbm_misc.say("[BUS MASTER MONITOR] Optimized Output queue layout is: ")
        self._neat_print_layout(self.layout, "Signalname", "Bit-Length")
        lbm_misc.say(f"[BUS MASTER MONITOR] Total bit-length per output entry is {sum([l for _,l in layout])}")



    def connect_signal_monitor_to_optimized_fifo(self, fifo_signal_list : List[Tuple[bool, Union[str, MigenValue], int]]):
        """Connect a signal from the SignalMonitor or from a provided value into the optimized output queue
            For each signal in the list when the bool is true, the signal gets switched, depending on which master had an active ack. If it is false and the name is a string, 
            this value gets read from the SignalMonitor.source.<name> queue output. If it is false and the name is a MigenValue the signal gets connected directly

        Args:
            fifo_signal_list (List[Tuple[bool, Union[str, MigenValue], int]]): The definition list
        """
        for differs_by_master, sig, _ in fifo_signal_list:
            
            if differs_by_master and type(sig) != str:
                print("A signal differing by master can only be internal to the signalMonitor queue and therefore only be referenced by a string and not a migen value")
                sys.exit()
            if differs_by_master:
                print(f"Adding signal {sig} to the selector!")
                self.case_dict["default"].append(self.active_master_fifo.sink.__getattr__(sig).eq(self.mon.source.__getattr__("0" + sig)))
                for i in range(len(self.master_interfaces)):
                    self.case_dict[2**i].append(self.active_master_fifo.sink.__getattr__(sig).eq(self.mon.source.__getattr__(str(i) + sig)))
            else:
                if type(sig) == str:
                    print(f"Connecting signal {sig} from the signalmonitor output queue")
                    self.comb += self.active_master_fifo.sink.__getattr__(sig).eq(self.mon.source.__getattr__(sig))
                elif type(sig) in MigenValueList:
                    print(f"Connecting signal {sig} directly to selector queue")
                    self.comb += self.active_master_fifo.sink.__getattr__(sig.name_override).eq(sig)
                else:
                    lbm_misc.say("Invalid type for the optimized fifo", error=True)
                    sys.exit()




    def get_converted_wb_interfaces(self, master_interfaces : List[Union[wishbone.Interface, AXILiteInterface, AXIInterface]]) -> List[wishbone.Interface]:
        tmp = []
        for idx, mint in master_interfaces:
            if type(mint) == wishbone.Interface:
                i = mint
            elif type(mint) == AXILiteInterface:
                i = self.axil2wb(mint)
            elif type(mint) == AXIInterface:
                i = self.axi2wb(mint)
            
            tmp.append((idx, i))
        return tmp


    
    def get_recorded_signal(self, signame):
        """Get a signal that is being recorded by its name (e.g. 3adr)"""
        return self.source.__getattr__(signame)
    
    def get_recorded_signals(self):
        """Return a list of all signals that are being recorded (in-order)"""
        return [self.get_recorded_signal(name) for name in self.get_recorded_signal_names()]
    
    def get_recording_layout(self):
        """Get the complete layout that goes out of this BusMasterMonitor"""
        return self.layout
    
    def get_recorded_signal_names(self):
        """Get the *names* of the signals being recorded"""
        return [signame for signame,_ in self.layout]
    
    def get_recorded_signal_lengths(self):
        return [l for _,l in self.layout]
    
    def get_complete_layout_width(self):
        """Width of the recording layout / the output queue"""
        return sum(self.get_recorded_signal_lengths())
    
    def axil2wb(self, interface: AXILiteInterface) -> wishbone.Interface:
        wb = wishbone.Interface(data_width=self.dma_bus.data_width, adr_width=self.dma_bus.address_width)
        self.submodules += AXILite2Wishbone(interface, wb)
        return wb

    def axi2wb(self, interface: AXIInterface) -> wishbone.Interface:
        wb = wishbone.Interface(data_width=self.dma_bus.data_width, adr_width=self.dma_bus.address_width)
        self.submodules += AXI2Wishbone(interface, wb)
        return wb

    def _neat_print_layout(self, layout, col1name, col2name, error=False):
        lbm_misc.say("-" * 42, error=error)
        lbm_misc.say("| {:<20} | {:<15} |".format(col1name, col2name), error=error)
        lbm_misc.say("-" * 42, error=error)
        for n,l in layout:
            lbm_misc.say("| {:<20} | {:<15} |".format(n,l), error=error)
        lbm_misc.say("-" * 42, error=error)
