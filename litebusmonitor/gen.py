#!/usr/bin/env python3

# This file is Copyright (c) 2014-2019 Florent Kermarrec <florent@enjoy-digital.fr>
# This file is Copyright (c) 2013-2014 Sebastien Bourdeauducq <sb@m-labs.hk>
# License: BSD

# Modified 05.05.2022

import os
import argparse
import importlib

from migen import *

from litex.build.io import CRG

from litex.soc.integration.soc_core import *
from litex.soc.integration.builder import *
from litex.soc.cores.uart import UARTWishboneBridge
from litex.soc.cores.led import LedChaser
from litex.soc.integration.soc import SoCRegion
from liteeth.phy import LiteEthPHY
from litex.soc.doc import generate_docs
from crg import _CRG

# Custom
#from monitor import LiteBusMonitor
#from receiver import LiteBusReceiver
from litex.soc.integration.builder import soc_directory, Builder
from lbm_misc import simple_connect
from litebusmonitor.bus_signal_monitors import BusMasterMonitor, SignalMonitor
from litebusmonitor.modules import MonitorDetector, ParityBitAdder
from reworked_monitor import MonitorSender, MonitorReceiver

class BaseSoC(SoCCore):
    def __init__(self, platform, **kwargs):
        sys_clk_freq = int(1e9 / platform.default_clk_period)
	# kwargs["uart_name"]="serial"

        # SoCCore ----------------------------------------------------------------------------------
        SoCCore.__init__(self, platform, sys_clk_freq,
                         ident="LiteX SoC featuring the VexRiscV softcore and the LiteBusMonitor monitoring core",
                         #cpu_variant="standard+debug",
                         **kwargs)

        # CRG --------------------------------------------------------------------------------------
        self.submodules.crg = _CRG(platform, sys_clk_freq)
        self.crg.cd_sys.clk.attr.add("keep")
        self.platform.add_period_constraint(self.crg.cd_sys.clk, 1e9 / sys_clk_freq)

       

        # IMPORTANT: When using this self.add_rom firmware set all linker sections except for .bss to firmware. If you DONT use this but programming per
        # Serial, set the linker sections to main_ram
        #self.add_rom("firmware", 0x30000000, size=25_000, contents=get_mem_data("firmware/firmware.bin", endianness="little"))
        #self.add_constant("ROM_BOOT_ADDRESS", 0x30000000)

    def add_monitor(self, fifodepth):
        detector_condition_list = []
 
        # BUS addresses are word addresses but memory regions store BYTE addresses
        
        
        # Add useful conditions that detect if a violation is detected
        
        # DBUS does an accepted write in ROM 
        detector_condition_list.append(
            lambda entry: (entry["1adr"] < (self.mem_regions["rom"].origin//4 + self.mem_regions["rom"].size//4)) & (entry["1we"] == 1) & (entry["1ack"] == 1)
        )

        # IBUS does an accepted write in ROM
        detector_condition_list.append(
            lambda entry: (entry["0adr"] < (self.mem_regions["rom"].origin//4 + self.mem_regions["rom"].size//4)) & (entry["0we"] == 1) & (entry["0ack"] == 1)
        )

        # The access has both an ACK and an ERR
        detector_condition_list.append(
            lambda entry: (entry["1ack"] & entry["1err"]) | (entry["0ack"] & entry["0err"])
        )

        # Both DBUS and IBUS ack
        detector_condition_list.append(
            lambda entry: (entry["0ack"] & entry["1ack"])
        )


        self.submodules.sender = sender = MonitorSender(self.bus, fifodepth, with_detector=True, detector_violation_led=self.platform.request("user_led", 7), detector_condition_list=detector_condition_list)
        self.submodules.receiver = receiver = MonitorReceiver(self, fifodepth, sender.bits_per_entry, self.bus)


        self.add_csr("sender")
        self.add_csr("receiver")
        self.add_interrupt("receiver", use_loc_if_exists=False)

        self.comb += [
            sender.source.ready.eq(receiver.sink.ready),
            receiver.sink.valid.eq(sender.source.valid),
            receiver.sink.data.eq(sender.source.data)
        ]


# Build --------------------------------------------------------------------------------------------

def main():
    parser = argparse.ArgumentParser(description="GDB over UART SoC")
    parser.add_argument("--build", action="store_true", help="Build bitstream")
    builder_args(parser)
    soc_core_args(parser)
    parser.add_argument("platform", help="Module name of the platform to build for")
    parser.add_argument("--toolchain", default=None, help="FPGA gateware toolchain used for build")

    parser.add_argument("--modbios", default=True, action="store_true", help="Call inserter.py to modify the bios with the given C snippets")

    parser.add_argument("--with-monitor", action="store_true", default=True, help="Setup a monitoring component that uses a dma to write entries to software")
    # 730 default entry count
    parser.add_argument("--monitor-elements", action="store", default="1024", help="Number of elements to save. Determines queue depth in design AND variable size in firmware")
    args = parser.parse_args()



    platform_module = importlib.import_module(args.platform)
    if args.toolchain is not None:
        platform = platform_module.Platform(toolchain=args.toolchain)
    else:
        platform = platform_module.Platform()
    soc = BaseSoC(platform, **soc_core_argdict(args))
    builder = Builder(soc, **builder_argdict(args))
    
    if args.with_monitor:
        soc.add_monitor(int(args.monitor_elements)) # Switch between 1 and 2

    if args.modbios:
        from software_insert.insert import insert_into_bios
        insert_into_bios()

    
    generate_docs(soc, "build/documentation")
    builder.build(run=args.build)
    
    #from litex.gen.fhdl.verilog import convert

    #print(convert(MonitorSender(), name="sender"))

if __name__ == "__main__":
    main()
