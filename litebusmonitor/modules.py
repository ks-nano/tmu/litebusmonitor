from migen import *
from migen.fhdl.structure import _Operator
from litex.soc.interconnect.stream import Endpoint, SyncFIFO
from typing import Callable, List, Tuple, Dict
from litebusmonitor.lbm_misc import Layout, MigenValue, say
import traceback
from litex.soc.interconnect.stream import *
from litex.soc.integration.doc import ModuleDoc
import sys


class ParityBitAdder(Module, ModuleDoc):
    """Accepts a queue (SyncFIFO) with a given layout as input and outputs a queue endpoint to self.source with the same layout, but one parity bit addded, which XORs all input bits.
    self.out_layout specifies the new layout after adapting the queue"""
    
    def __init__(self, layout : Layout, input_fifo : SyncFIFO) -> None:
        XOR = lambda x,y: _Operator("^", [x,y])
        self.out_layout = out_layout = layout + [("parity", 1)]

        
        # TODO: Change so that not the queue gets passed, but connected via a sink endpoint
        
        internal_queue = SyncFIFO(out_layout, 8)
        self.submodules += internal_queue

        # Connect input fifo to internal temporary fifo
        self.comb += [
            input_fifo.source.ready.eq(internal_queue.sink.ready),
            internal_queue.sink.valid.eq(input_fifo.source.valid)
        ]

        # Connect all normal values straight through
        for n,_ in layout:
            self.comb += internal_queue.sink.__getattr__(n).eq(input_fifo.source.__getattr__(n))

        # Add parity signal to temporary queue
        self.comb += internal_queue.sink.parity.eq(reduce(XOR, input_fifo.source.raw_bits()))

        
        self.source = internal_queue.source





class MonitorDetector(Module, ModuleDoc):
    """Creates a module that detects violations of given policies. An example for a condition/policy: The first master is not allowed to write into the first address space:
        condition = lambda entry: entry["adr"] < 0x500 & entry["we"] == 1 & entry["acknowledged_master"] == 0
    """
    def __init__(self, signal_dict : Dict, signals_valid : Signal, detector_conditions : List[Callable], violation_detected_led) -> None:

        # Set a flag to 1 as soon as a violation was detected
        self.violation_detected = Signal(reset=0)
        self.comb += violation_detected_led.eq(self.violation_detected)
        
        # Warn if any of the queue elements are dangerous
        for condition in detector_conditions:
            try:
                self.sync += [
                    If(signals_valid & condition(signal_dict),
                        self.violation_detected.eq(1)
                    )
                ]
            except KeyError:
                say(f"[DETECTOR] A signal was not found in the signal dict:", error=True)
                say(f"The list of available signals is: {list(signal_dict.keys())}")
                print(traceback.format_exc())
                sys.exit()
        