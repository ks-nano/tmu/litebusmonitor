from asyncore import write
from migen import *
from litex.soc.interconnect import wishbone # type: ignore
from litex.soc.cores.dma import WishboneDMAWriter # type: ignore
from typing import Union, List, Optional
from litebusmonitor.lbm_misc import * 
from litex.soc.integration.soc import * # type: ignore
from litex.soc.interconnect.stream import *
from litebusmonitor.bus_signal_monitors import BusMasterMonitor
from litex.soc.integration.doc import ModuleDoc
import json
from numpy import lcm
from math import ceil


class Connectable(Module, AutoCSR, ModuleDoc):
    """Docs"""
    def simple_connect(self, a, b, aSourceReady=None, bSinkValid=None, dataInput=None):
        """Connects A -> B. If the source-ready or sink-valid or data signals should come from another source other than A or B you can pass them""" 
        self.comb += [
            a.source.ready.eq(b.sink.ready if aSourceReady is None else aSourceReady),
            b.sink.valid.eq(a.source.valid if bSinkValid is None else bSinkValid),
            (b.sink.data.eq(a.source.data) if dataInput is None else (b.sink.data.eq(dataInput)))
        ]



class MonitorSender(Connectable, AutoCSR, ModuleDoc):
    """MonitorSender
    
    This module acts as a buffer and records signals from the SoCs bus for analysis. The module gets passed a SoCBusHandler or a list of wishbone.Interfaces. The masters that
    write on that bus get collected and listened to. When the bus issues an ``ack`` to the master interface, the current values of the interface get recorded and packed as an entry.
    The signals that get recorded by default are:


    * ``address``
    * ``we``
    * ``sel``
    * ``ack``
    * ``err``
    
    
    More signals can be added to entries by configuring this module.

    The ouput of the buffer queue gets split into 32-bit chunks and output through ``self.source``. It can thus be easily connected to a ``MonitorReceiver`` modules' sink.

    Parameters
    ----------

    target : Union[SoCBusHandler, List[wishbone.Interface]]
        The target bus or list of target masters to record
    
    buffered_entries : int
        The number of entries to buffer. How many you can buffer is dependent on the number of bits an entry consumes and how much BRAM is available on your board

    select_by_ack : bool = True
        Whether or not to select before making an entry. Internally all busses get recorded at the same time when only one ack is seen. But since normally the data of not-acked
        bus masters is invalid, you can save atleast half of the memory by simply ignoring these signals. This adds an attribute to the entry that specifies which master was confirmed
    
    with_detector : bool = True
        Whether or not to add a detector module. This module alerts a Signal in case a violation of specified policies was detected, immediatly when recording an entry
    
    detector_violation_led : Any
        A Signal or LED Signal to be set to high when a violation was detected
    
    detector_condition_list : List[Callable]
        A list of lambdas that receive a dictionary of all signals that are recorded and specify conditions on those signals. When a condition hits and the entry is valid,
        the specified signal gets set to high to alert another part of the SoC

    add_bus_idle_cycle_counter : bool = True
        Whether to add a signal that counts the cycles in which nothing was recorded. This is refered to as "csla" (cycles since last acknowledge). Reset after every ack

    add_trigger_counter : bool = True
        Counts the times how often an entry was added

    add_recorder_queue_level_counter : bool = True
        Saves at what level the buffer recorder queue was when the entry was recorded
    
    add_interrupt_counter : bool = True
        Saves the number of times the interrupt was triggered
    """
    def __init__(
        self, 
        target: Union[SoCBusHandler, List[wishbone.Interface]], 
        buffered_entries : int,
        select_by_ack : bool = True,
        with_detector=True, detector_violation_led=None, detector_condition_list=None,
        add_bus_idle_cycle_counter=True, add_trigger_counter=True, add_recorder_queue_level_counter=True, add_interrupt_counter=True
        ) -> None:

        # CSR
        self.lock_fifo_csr = CSRStorage(1, reset=0, name="lock_fifo")

        # Monitor
        self.bm = bm = BusMasterMonitor(list(target.masters.values()) if type(target) == SoCBusHandler else target, entries=buffered_entries, select_by_ack=select_by_ack,
        with_detector=with_detector, detector_violation_led=detector_violation_led, detector_condition_list=detector_condition_list,
        add_bus_idle_cycle_counter=add_bus_idle_cycle_counter, add_interrupt_counter=add_interrupt_counter,
        add_trigger_counter=add_trigger_counter, add_recorder_queue_level_counter=add_recorder_queue_level_counter)
        self.submodules += self.bm
        
        tmp_bits_per_entry = bits_per_entry = bm.get_complete_layout_width()


        # Pad the queue data to the nearest multiple of 32 and fill rest with 0s
        padded_length = tmp_bits_per_entry + (32 - (tmp_bits_per_entry % 32))

        # Bits per entry attribute contains the padding!
        self.bits_per_entry = padded_length
        entry_signal = Signal(padded_length)

        say(f"Original Entry Length: {bits_per_entry}\nWith padding: {padded_length}")

        # Converter
        self.submodules.converter = converter = Converter(padded_length, 32, reverse=True)

        # Pipeline
        self.comb += [
            # Lock FIFO if csr says so
            bm.mon.lock_fifo.eq(self.lock_fifo_csr.storage),
            
            # Concatted signal from the monitors queue with padded zeros in the MSB spots (00000...000DATA)
            entry_signal.eq(Cat(Cat(bm.get_recorded_signals()[::-1]), Replicate(0, padded_length-bits_per_entry))),
        ]

        # FIFO -> converterGate -> converter -> senderOutGate
        self.simple_connect(self.bm, converter, dataInput=entry_signal)
        self.source = converter.source

        # Write config out for the decoder to read
        self.write_layout_config_files(padded_length - bits_per_entry)

        self.layout : Layout = bm.get_recording_layout()

    
    def write_layout_config_files(self, leadingZeros, foldername="build"):
        layout = {"padding": leadingZeros}
        for n,l in self.bm.get_recording_layout():
            layout[n] = l

        nearest_two_power = lambda x: 2**ceil(log2(x))

        with open(os.path.join(foldername, "layout_config.json"), 'w+') as f:
            json.dump(layout, f)
        
        with open(os.path.join(foldername, "layout_config.h"), 'w+') as f:
            s = f"""
#include <stdint.h>
typedef union __attribute__((__packed__))
bitfield_t {{
    #pragma scalar_storage_order big-endian
    struct __attribute__((__packed__))
    {{
"""
            
            for n,l in layout.items():
                s += f"\t\tuint32_t {n} : {l};\n"
            
            s += f"""
    }};
    uint8_t data[{(self.bm.get_complete_layout_width()+leadingZeros)//8}];
    uint64_t data64;
}} bitfield;

#define CSV_PRINT(fp_out, bf, broken) fprintf(fp_out, "{','.join(['%u']*len(layout.items()))},%u\\n", {','.join(['bf.%s' % n for n,l in layout.items()])},broken);

"""

            f.write(s)



class MonitorReceiver(Connectable, AutoCSR, ModuleDoc):
    """MonitorReceiver

    The MonitorReceiver is a module that accepts stream input to self.sink with a payload of ``[("data", 32)]``. It creates a wishbone interface to write on the bus and uses
    this to write the entries received in the sink into memory using a DMA. When a certain threshold of memory space was written (by default 50%) an interrupt is issued. The
    module waits until the interrupt and used memory are cleared to resume writing data. When connected to a sender this thus also stops the clearing of the recording queue.

    *Don't forget to add the interrupt and csr's to your soc after adding the receiver to submodules:*
    ``self.add_csr("sender")``, ``self.add_csr("receiver")``, ``self.add_interrupt("receiver", use_loc_if_exists=False)``

    Parameters
    ----------

    soc : SoC
        A reference to the SoC that this receiver is added to. Needed to set macros in the LiteX generated header files
    
    dma_entries : int
        How many entries should be saved per DMA per run. Note that this does **not** have to be same number of entries that are buffered by the sender. If a large memory
        is available it should be used to avoid ever congesting the sender buffer
    
    bits_per_entry : int
        This has to be passed from the sender (or the programmer when set at build-time) and represents how many bits an entry has. This is used to trigger the interrupt at the
        proper time so that reading the memory, the entry does not get split in half because writing was stopped in the middle.

    dma_bus : wishbone.Interface
        The bus interface that is used to communicate with the SoCs bus (or the specific one used to write the entries)
    
    dma_starting_address : int = None
        If an address to write entries to is known at build time it can be passed here. If set it prepares the DMA, and sets macros for the BIOS to not define memory itself
    
    mem_filled_intr_percent : float = 0.5
        At which percentage of full memory the interrupt should be triggered. This should not be too low in order to avoid waste of memory but also not too high to avoid that 
        entries get lost when the DMA is full but isnt getting cleared yet

    """
    def __init__(self, soc : SoC, dma_entries : int, bits_per_entry : int, dma_bus : wishbone.Interface, dma_starting_address : int = None,
    mem_filled_intr_percent : float = 0.5) -> None:

        assert dma_bus.data_width == 32

        # Calculate where interrupts are supposed to happen
        self.words_needed = int(ceil(dma_entries * bits_per_entry / 32)) # Assuming 32 bit words
        
        break_every = bits_per_entry / 32
        break_at_word = int(int(ceil(((int(self.words_needed * mem_filled_intr_percent)) / break_every) + 1)) * break_every) # The next multiple of break_every above the half way mark of the DMA
        say(f"The DMA will interrupt at {break_at_word} 32 bit words")

        # Write a define macro to soc.h which can be read at build time to generate an array of the appropiate size for the readout of the DMA
        soc.constants.update({"MONITOR_WORDS": self.words_needed})
        soc.constants.update({"UART_POLLING": 1})
        if dma_starting_address is not None:
            soc.constants.update({"MONITOR_DMA_STARTING_ADDRESS_PRESET": None})
        
        # Declarations (Gate3, DMA, CSRs)
        self.submodules.input_gate = input_gate = Gate([("data", 32)])
        self.enable_dma_csr = CSRStorage(1, write_from_dev=True, reset=int(dma_starting_address is not None), name="enable_dma")
        self.resume_csr = CSRStorage(1, name="resume", write_from_dev=True)
        self.dma_address_set_csr = CSRStorage(1, name="dma_adr_set", reset=int(dma_starting_address is not None), write_from_dev=True)
        
        # DMA
        self.create_dma(dma_bus, dma_starting_address)

        # FIXME: Introduce one cycle delay because of LiteX issue 
        self.enable_dma_delay = Signal(1, reset=0)
        self.enable_dma_delay0 = Signal(1, reset=0)
        self.sync += [
            self.enable_dma_delay0.eq(self.enable_dma_csr.storage), # The delay blocks the gate until the DMA is ready, the DMA gets readied instantly
            self.enable_dma_delay.eq(self.enable_dma_delay0),
            self.dma._enable.storage.eq(self.enable_dma_csr.storage)
        ]

        # Breaking when an interrupt should be done
        BREAK_SIGNAL = Signal(1, reset=0)
        self.sync += [
            If(~BREAK_SIGNAL,
                If(self.dma._offset.status == break_at_word, 
                    BREAK_SIGNAL.eq(1)
                )
            ).Else(
                If(self.resume_csr.storage,
                    BREAK_SIGNAL.eq(0),
                    #self.resume_csr.storage.eq(0)
                )
            )
        ]

        self.comb += [
            input_gate.enable.eq(~BREAK_SIGNAL & self.dma_address_set_csr.storage)
        ]

        # Piping input into gate
        self.sink = input_gate.sink

        self.simple_connect(input_gate, self.dma)

        # Interrupt line
        self.submodules.ev = EventManager()
        self.ev.half_full_mem_int = EventSourceProcess(edge="rising")
        self.ev.finalize()

        # Trigger when break value is reached: a multiple of 32 left the converter so that its internal shift register is empty
        self.comb += self.ev.half_full_mem_int.trigger.eq(BREAK_SIGNAL)   


    def create_dma(self, bus, startaddr):
        # DMA Length is in BYTE, and DMA offset is in WORDS (32 bit)   
        self.dmaInterface = wishbone.Interface(data_width=bus.data_width, adr_width=bus.address_width)
        bus.add_master(master=self.dmaInterface, name="MonitorReceiverDMAMaster")
        self.submodules.dma = self.dma = WishboneDMAWriter(self.dmaInterface, endianness="big")
        self.dma.add_csr(default_base=(0 if startaddr is None else startaddr), default_length=self.words_needed*4, default_enable=0, default_loop=0) # Has to start disabled or contents get written to address 0 at first
        say(f"DMA CREATED:\nLENGTH:{self.words_needed} words or {self.words_needed*4} bytes")


