// INSERT_START after_declarations
// First enable interrupts, only then start DMA so that the memory-half-full ISR gets called in any case
#ifdef CONFIG_CPU_HAS_INTERRUPT

    #ifndef RECEIVER_INTERRUPT
        #define RECEIVER_INTERRUPT 1
    #endif

    irq_setmask(irq_getmask() | (1 << RECEIVER_INTERRUPT));
    irq_setie(1);
    receiver_ev_enable_write(1);
#endif

init_monitor();
// INSERT_END after_declarations

// INSERT_START initdef
void init_monitor() {
    // If the address was not set when building the hardware, it still has to be defined and passed
    #ifndef MONITOR_DMA_STARTING_ADDRESS_PRESET
	    receiver_dma_base_write(fifoContents);	// Set DMA base address
        receiver_enable_dma_write(1);
        receiver_dma_adr_set_write(1);
    #endif
}
// INSERT_END initdef
