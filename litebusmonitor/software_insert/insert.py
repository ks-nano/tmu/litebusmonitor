# Currently there is no good way to inject code early in the boot process using LiteX. This script takes the folder structure of this directory, which mirrors
# the litex .../soc/software directory and uses the filenames to copy the contents of these files into the marked spots of the original litex software dir

# The c/h files in this dir contain the C code embedded in two comments:
#
# // INSERT_START MyInsert
# int myValue = 4;
# printf("%d\n", myValue);
# // INSERT_END MyInsert
#
# Enumerating the comments or naming them. This same pattern is used to mark the insert in the target file

from typing import Dict
from litex.soc.integration.builder import soc_directory
import os, sys

IGNORE_NAMES = ["insert.py", "__pycache__"]

def extract_code_snippets(fname : str) -> Dict[str, str]:
    snippets = {}
    current_snippet = ""
    current_snippet_index = ""
    recording = False
    with open(fname, 'r') as f:
        try:
            for line in f:
                if line.startswith("// INSERT_START"):
                    current_snippet = ""
                    current_snippet_index = line.split()[2]
                    recording = True
                elif line.startswith("// INSERT_END"):
                    if not line.split()[2] == current_snippet_index:
                        print(f"Bad insert formatting! Wrong closing statement. Must match INSERT_START name!")
                        sys.exit()
                    snippets[current_snippet_index] = current_snippet
                    recording = False
                
                elif recording:
                    current_snippet += line
        except IndexError:
            print("Error: Every INSERT_START/END statement needs an index/name following it!")
            sys.exit()
    return snippets            


def insert_snippets(target_fname, snippets):
    fbuffer = ""
    end_found = True
    inserted = 0
    with open(target_fname, 'r') as f:
        try:
            for line in f:
                if line.startswith("// INSERT_START"):
                    index = line.split()[2]
                    if index not in snippets.keys():
                        print(f"Index/Name {index} found in target file but not in insertion/snippet file. Exiting.")
                        sys.exit()
                    end_found = False
                    fbuffer += line
                    fbuffer += snippets[index]
                    inserted += 1
                elif line.startswith("// INSERT_END"):
                    end_found = True
                    fbuffer += line
                elif end_found:
                    # This avoids adding lines between the insert start and end except for the snippet, overriding anything that was there before
                    fbuffer += line
        except IndexError:
            print("Error: Every INSERT_START/END statement needs an index/name following it!")
            sys.exit()
        
        # Writeback
        with open(target_fname, 'w+') as f:
            f.write(fbuffer)
        
        print(f"\tInserted {inserted} of {len(snippets.keys())} snippets!")



def insert_into_bios():
    SOFTWARE_DIR = os.path.join(soc_directory, "software")
    print("Modifying LiteX BIOS Code...")
    file = os.path.dirname(__file__)
    for fileobject in os.listdir(file):
        print(f"Inserting {fileobject}...")
        if fileobject not in IGNORE_NAMES:
            if os.path.isfile(fileobject):
                print("Found file.")
                if not fileobject in os.listdir(os.path.join(SOFTWARE_DIR)):
                    print(f"File {fileobject} not found in /litex/soc/software directory. Skipping.")
                    continue
            else:
                print("Found folder.")
                # Make sure folder also exists in software dir
                if not fileobject in os.listdir(SOFTWARE_DIR):
                    print(f"Folder {fileobject} not found in the /litex/soc/software directory. Skipping.")
                    continue

                # Make sure all files in that folder exist in the target dir 
                files_to_copy = [fn for fn in os.listdir(os.path.join(file, fileobject)) if os.path.isfile(os.path.join(file, fileobject, fn))]
                for ftc in files_to_copy:
                    if ftc not in os.listdir(os.path.join(SOFTWARE_DIR, fileobject)):
                        print(f"File {ftc} not found in the /litex/soc/software/{fileobject} directory. Skipping.")
                        continue

                for ftc in files_to_copy:
                    print(f"Inserting file {fileobject}/{ftc}...")
                    print(f"\tExtracting snippets from {ftc}")
                    snippets = extract_code_snippets(os.path.join(file, fileobject, ftc))
                    print(f"\tInserting snippets to target {ftc}")
                    insert_snippets(os.path.join(SOFTWARE_DIR, fileobject, ftc), snippets)
                    print("\tDone!\n")
            
if __name__ == "__main__":
    insert_into_bios()

