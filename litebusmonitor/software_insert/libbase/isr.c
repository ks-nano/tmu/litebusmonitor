// INSERT_START isrchead
#include <math.h>
#include "isr.h"

#ifndef MONITOR_DMA_STARTING_ADDRESS_PRESET
	volatile unsigned long fifoContents[MONITOR_WORDS] = {0};
#endif

int firstInterrupt = 1;
// INSERT_END isrchead

// INSERT_START isrcfuncdefs
void print_entries(volatile unsigned long *contents) {
    dcache_clear();

	unsigned int writer_offset = receiver_dma_offset_read();

    // Print starting symbol
    uart_write('\n');
	for (int i = 0; i < 10; i++) {
		uart_write('M');
    }
    uart_write('\n');

    // Print length of words to read
	uart_write((char)(writer_offset>>24) & 0xFF);
    uart_write((char)(writer_offset>>16) & 0xFF);
    uart_write((char)(writer_offset>>8) & 0xFF);
    uart_write((char)(writer_offset) & 0xFF);
	
    // Print all bytes
    for (int i = 0; i < writer_offset; i++) {
        uart_write((char)(contents[i]>>24) & 0xFF);
        uart_write((char)(contents[i]>>16) & 0xFF);
        uart_write((char)(contents[i]>>8) & 0xFF);
        uart_write((char)(contents[i]) & 0xFF);
    }

    // Print end code
	for (int i = 0; i < 10; i++) {
		uart_write('E');

    }
    uart_write('\n');
}

void ready_isr() {
	// Called at the Start of every ISR
	sender_lock_fifo_write(1);
}

void unready_isr() {	
	receiver_enable_dma_write(0);	
	receiver_enable_dma_write(1);
	sender_lock_fifo_write(0);
	receiver_resume_write(1);
	receiver_resume_write(0);
}
// INSERT_END isrcfuncdefs

// INSERT_START isr
void isr(void)
{
	__attribute__((unused)) unsigned int irqs;
	irqs = irq_pending() & irq_getmask();
    #ifdef CSR_UART_BASE
        #ifndef UART_POLLING
	        if(irqs & (1 << UART_INTERRUPT))
		        uart_isr();
        #endif
	    #ifdef RECEIVER_INTERRUPT
		    if (irqs & (1 << RECEIVER_INTERRUPT)) {
                ready_isr();
			    monitor_isr();
                unready_isr();
            }
	    #endif
    #endif
}

void monitor_isr(void) {
	printf("TEST\n\n\n\n");
	if (firstInterrupt == 1) {
        printf(" \n\n\n");
		printf("\nSOC_START\n");
		firstInterrupt = 0;
	}

	unsigned int status = receiver_ev_pending_read();
	if (status & 1) {
		print_entries(fifoContents);
		receiver_ev_pending_write(1);
	}
	receiver_ev_enable_write(1); // So that the EV can interrupt again
}
// INSERT_END isr