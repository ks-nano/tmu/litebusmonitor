// INSERT_START isrh
#include <generated/soc.h>
#ifndef PREDEFINED_H
#define PREDEFINED_H

#ifndef MONITOR_DMA_STARTING_ADDRESS_PRESET
    extern volatile unsigned long fifoContents[MONITOR_WORDS];
#else
    volatile unsigned long* fifoContents = receiver_dma_base_read();
#endif

#endif

// INSERT_END isrh