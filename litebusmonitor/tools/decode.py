import argparse
from bdb import Breakpoint
import json
from operator import is_
import sys
from bitstring import BitArray, ConstBitStream
from typing import List, Tuple, Dict
import time
from tqdm import tqdm
import numpy as np
import serial
import os
import pandas as pd
from datetime import date

OUTPUT_FOLDER = os.path.join("..", "build")
TODAY = str(date.today()).replace("-", "_")

parser = argparse.ArgumentParser()

parser.add_argument("--filename", "-f", action="store", default=os.path.join(OUTPUT_FOLDER, f"raw_data_{TODAY}.bin"))
parser.add_argument("--delimiter", "-d", action="store", default="######")
parser.add_argument("--configfile", default=os.path.join(OUTPUT_FOLDER, "layout_config.json"))

parser.add_argument("--record", default=True, action="store_true", help="Whether to record new data into the bin file before decoding it")
parser.add_argument("--breakafterboot", default=True, action="store_true", help="Break recording when the litex boot string was read")

parser.add_argument("--baudrate", default="1000000", help="Baudrate for serial", action="store")
parser.add_argument("--port", default="/dev/ttyUSB1", action="store")
parser.add_argument("--blocks", default=10, help="0: Until Ctrl+C, n: n Blocks", action="store")
args = parser.parse_args()




# 1. Ack and Err both false happens 10 000s of times when firingMaster=0 but only handful of times when fm = 1
# 2. The first occurrence of this is the first entry of the third block
# Why is the first block complete but the others arent? 








if args.record:
    if args.breakafterboot:
        print("Recording is going to cancel when litex boot string was detected: sL5DdSMmkekro")
        print()
        
    ser = serial.Serial(args.port, baudrate=int(args.baudrate))
    print(f"PORT: {args.port}\nBAUDRATE: {args.baudrate}\n")
    print("Waiting for SOC_START signal!")
    line = ""
    previous_line = ""

    while line != "\rSOC_START\n":
        try:
            #ser.write(b"boot 0x30000000\n")
            line = ser.readline().decode()
        except UnicodeDecodeError:
            continue


    
    print("Starting recording...\n")
    
    is_recording = False
    just_started = False

    with open(os.path.join(OUTPUT_FOLDER, f"output_log_{TODAY}.txt"), 'w+') as tf: 
        with open(os.path.join(OUTPUT_FOLDER, f"raw_data_{TODAY}.bin"), 'wb+') as f:
            for currentBlock in tqdm(range(int(args.blocks))):
                while True:
                    line = ser.readline()
                    #print("L")
                    
                    if b"ekro" in line:
                        print(f"FINISHED BOOT AT BLOCK {currentBlock}")

                   # print(f"L {line}")
                    if line not in [b"\r\n", b"MMMMMMMMMM\n"]:
                        try:
                            tf.write(line.decode())
                        except UnicodeDecodeError:
                            pass
                    if line == b"MMMMMMMMMM\n":
                        # Read how many bytes to read
                        #bcs = ser.readline().decode()
                        bcs = BitArray(ser.read(4)).uint
                        bytes_count = int(bcs)*4

                        # Read contents (cut off first byte, which is always \r)
                        content = ser.read(bytes_count)
                        
                        #f.write(b"\x00" + content)
                        f.write(content)

                        # Write delimiter
                        f.write(args.delimiter.encode())

                        tmp = ser.readline()
                        assert tmp ==  b"EEEEEEEEEE\n", f"{tmp}"
                        break



if args.configfile == "":
    config = [
        ("firingMaster", 1),
        ("adr", 30),
        ("we", 1),
        ("sel", 4),
        ("ack", 1),
        ("err", 1),
        ("clsa", 16)
    ]
else:
    print("Loading config from file")
    with open(args.configfile, 'r') as cf:
        config = json.load(cf).items()


print("Config: ")
for name, l in config:
    print(f"{name} - {l}")
print()


def insert_entry(config : List[Tuple[str, int]], entry : ConstBitStream, entryarray : np.ndarray, entryindex : int) -> List[int]:
    # entryarray large block array
    # entryindex index which entry in the block array gets written
    # i is the index IN that row
    current = 0
    i = 0
    for _, length in config:
        entryarray[entryindex][i] = entry[current : current + length].read(f"uint:{length}")
        current += length
        i += 1


entrybits = sum([l for _,l in config])

with open(args.filename, 'rb') as f:
    contents = f.read()

# ignore last block (is empty)
blocks = contents.split(args.delimiter.encode())[:-1]



print("Decoding..")
entries = []
smallest_block = 2200
largest_block = 0
first = True
#all_entries = np.array([], dtype=[("padding", np.uint64), ("firingMaster", np.byte), ("address", np.uint32), ("we", np.bool_), ("sel", np.byte), ("ack", np.bool_), ("err", np.bool_), ("csla", np.uint16)])
all_entries = np.array([], dtype=[(name, np.uint64) for name,_ in config])


block_sizes = []

for block in tqdm(blocks):
    # Truncate first byte (carriage return) and last not entry aligned bits (doenst truncate anything at the end if the bits are a multiple of the entry bit width)
    bitblock = ConstBitStream(bytes=block)
    bitblock = bitblock[:len(bitblock)-(len(bitblock)%entrybits)]
    
    # Reserve memory for decoding
    entries_in_block = len(bitblock)//entrybits
    block_entries = np.zeros(entries_in_block, dtype=[(name, np.uint64) for name,_ in config])

    smallest_block = min(smallest_block, len(bitblock))
    largest_block = max(largest_block, len(bitblock))

    # Insert all entries in this block into the array
    for i in range(len(bitblock)//entrybits):
        entry = bitblock[i * entrybits : (i+1) * entrybits]
        insert_entry(config, entry, block_entries, i)

    all_entries = np.append(all_entries, block_entries)
    block_sizes.append(entries_in_block)


print(f"Sizes of first 10 blocks: {block_sizes}")

print(f"Smallest block read (entries): {smallest_block/entrybits}\nLargest block read(entries): {largest_block/entrybits}")

print("Saving entries...")
print("Preview:")
df = pd.DataFrame(all_entries)
print(df.head())
print()
print()

print("Ack AND Err false, fm 0")
print(df[(df["ack"] == False) & (df["err"] == False) & (df["acknowledged_master"] == 0)])
print("Ack AND Err false, fm 1")
print(df[(df["ack"] == False) & (df["err"] == False) & (df["acknowledged_master"] == 1)])

print("Entries where addr!=0")
print(df[(df["adr"] != 0)])

print()
print()
print()
print("non allowed (dbus writes in rom)")
print(df[(df["we"] == 1) & (df["adr"] < 64000)])
print()
print()
print("some")
print(df[2150:2200])





pd.set_option('display.max_rows', 1600)
#print(df[:1600])



print("Writing...")
df.to_feather(os.path.join(OUTPUT_FOLDER, f"data_feather_{TODAY}.arrow"))
