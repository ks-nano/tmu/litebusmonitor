//use serialport::*;
use std::{time::Duration, io::Read};
use clap::{Parser};
use std::fs;
use std::fs::OpenOptions;
use std::io::*;
use std::collections::HashMap;
use serde_json::{Value, Map};
use std::io::BufReader;
use std::sync::mpsc;
use std::sync::mpsc::Sender;
use std::thread;
use std::str;
use packed_struct::prelude::*;


#[derive(Parser,Default,Debug)]
#[clap(about)]
struct Arguments {
    // Mode to start in. s for serial and decode, and f for only decode (on file)
    #[clap(short, long, value_parser, default_value_t = 's')]
    mode : char,

    // Either the port to listen to with serial or the path to the file to read
    #[clap(short='p')]
    #[clap(long="port-or-path", value_parser, default_value_t = String::from("/dev/ttyUSB1"))]
    path : String,

    #[clap(long="baudrate", value_parser, default_value_t = 5_000_000)]
    baudrate : u32,

    // The delimiter that splits blocks when reading from a file
    #[clap(short, long, default_value_t = String::from("\n###"), value_parser)]
    delimiter : String,

    #[clap(short, long="layout", default_value_t = String::from("../layout_config.json"), value_parser)]
    layout_config_path : String,

    // How many blocks to read
    #[clap(long, default_value_t = 10, value_parser)]
    blocks : i32
}

/* 
#[derive(PackedStruct)]
#[packed_struct(bit_numbering="msb0")]
struct Entry {
    // TODO: Create struct elements from json config file
}
*/

type Byteblock = Vec<u8>;

#[derive(Debug)]
struct LayoutConfig  {
    filepath : String,
    layout : HashMap<String, i32>,
    bits_per_entry : i32
}

impl LayoutConfig {
    fn new(fp : &str) -> LayoutConfig {
        let content : String = fs::read_to_string(fp).expect("Error during reading of file. Maybe wrong path?");
        let map : HashMap<String, i32> = serde_json::from_str(&content).expect("Error when parsing JSON into type HashMap<String,i32>");
        let bitsum = map.values().sum();
        LayoutConfig { filepath: String::from(fp), layout: map, bits_per_entry: bitsum }
    }
}



// Takes config parameters for serial and sends the bytes read from serial into a channel to another thread for decoding
fn record_with_delimiter(port : String, baudrate : u32, blocks : i32, tx : Sender<Vec<u8>>) {
    println!("Serial port connected at {} with a baudrate of {}", port, baudrate);
    let mut port = serialport::new(port, baudrate).timeout(Duration::from_millis(100)).open().expect("Failed to open serial port!");
    println!("Reading {} blocks.\nWaiting for SOC_START symbol", blocks);

    let mut reader = BufReader::new(port);
    let mut current_line : Vec<u8> = Vec::new();
    let mut s = "";
    
    let mut started : bool = false;
    let mut recording : bool = false;  

    let mut current_block = 1;
    while current_block != blocks+1 {
        current_line = Vec::new();
        
        //reader.read_until("\n".as_bytes()[0], &mut current_line).unwrap();
        reader.read_until(0xA, &mut current_line).unwrap();
        s = match str::from_utf8(&current_line) {
            Ok(bs) => bs,
            Err(es) => ""
        };

        if s == "\rSOC_START\n" {
            started = true;
        } else if started && !recording && s == "\rMONITOR_DATA_START\n" {
            recording = true;
        } else if started && recording && s == "\rMONITOR_DATA_END\n" {
            recording = false;
            current_block += 1;
            println!("Recorded block {}", current_block);
        } else if started && recording {

            //TODO: This appends the \n as well because read_until doesnt remove the delimiter
            tx.send(current_line).expect("Error when sending data to main thread");
        }
    }
}


// Read from binary file with the given delimiter and return these as byte vectors
fn read_blocks_from_file(fp : &str, delimiter : &str) -> Vec<Byteblock> {
    let mut mainvec : Vec<Vec<u8>> = Vec::new();
    let mut file = fs::File::open(fp).unwrap();
    let mut temp : String = String::new();
    file.read_to_string(&mut temp).unwrap();
    for strblock in temp.split(delimiter) {
        mainvec.push(String::from(strblock).into_bytes());
    }
    mainvec
}

fn main() {
    let cli = Arguments::parse();

    // Args for reading serial
    let port = cli.path;
    let baudrate = cli.baudrate;
    let delim : &str = "\n###";
    let blocks = cli.blocks;

    
    println!("Mode: {}\nPort/Path: {}\nBaudrate: {}\nBlocks:{}\nDelimiter: {}\n", cli.mode, port, baudrate, blocks, delim);

    // Parse config JSON
    //let conf : LayoutConfig = LayoutConfig::new(&cli.layout_config_path);

    let mode = cli.mode;

    if mode == 's' {
        println!("Reading and saving from serial...");
        // Main thread: decode
        // Spawned thread: read from serial
        let (tx, rx) = mpsc::channel();

        // One thread records from serial and sends the data as bytes, cleared 
        let handle = thread::spawn(move || {
            record_with_delimiter(port, baudrate, blocks, tx);
        });
        
        // Main thread handles writing into a file
        let mut file = OpenOptions::new().write(true).create(true).open("raw_data_rs.bin").expect("Error saving byte data");

        // Handle the bytes
        for block in rx {
           file.write(&block).expect("Error writing raw bytes to file");
           file.write(delim.as_bytes()).expect("Error writing delimiter bytes to file");
        }



















        handle.join().unwrap();









    // TODO: Filemode later
    } else if mode == 'f' {
       // Read blocks from file
       let blocks : Vec<Byteblock> = read_blocks_from_file(&port, delim); 
    }

}
