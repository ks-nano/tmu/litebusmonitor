# Script to simplify usage of the monitor
import argparse
import os
from termcolor import colored

def run():
    pass

def make_uart_polling(uartpath):
    print(colored("[UART-POLLING] ", "green") + "Setting uart to polling mode")
    buffer = ""
    with open(uartpath, 'r') as f:
        for line in f:
            if "#define UART_POLLING" in line:
                print(colored("[UART-POLLING] ", "green") + "Already set to polling. Nothing changed.")
                return
            if "#ifndef UART_POLLING" in line:
                buffer += "#define UART_POLLING\n" + line
            else:
                buffer += line
    print(colored("[UART-POLLING] ", "green") + "Writeback with correct macro set...")
    with open(uartpath, 'w+') as f:
        f.write(buffer)

def create_litex_env_vars():
    # Source home rc dotfile
    bashrcsource = "source ~/.bashrc"
    a = input(colored("[SETUP/BASHRC] ", "green") + "Do you want to source your .bashrc for the build scripts? (y = yes, n = no, or write your source command otherwise) > ")
    if a.lower() == "n":
        bashrcsource = ""
    elif a.lower() != "y":
        bashrcsource = a

    # LiteX directory path
    litexdir = ""
    try:
        from litex.soc.integration.builder import soc_directory
        litexdir = os.path.dirname(soc_directory)
    except ImportError:
        print(colored("[SETUP/LITEX-DIRECTORY] ", "orange") +  "Could not import LiteX directory from litex module. Please enter the correct directory!")
        litexdir = "?"
    
    a = input(colored("[SETUP/LITEX-DIRECTORY] ", "green") + f"If {litexdir} is your LiteX directory (containing soc, etc.) press ENTER, otherwise input the correct path > ")
    if a != "":
        litexdir = a
    
    # LBM SOC Dir
    lbmsocdir = os.path.dirname(os.getcwd())
    a = input(colored("[SETUP/LITEBUSMONITOR-DIRECTORY] ", "green") + f"Is your litebusmonitor soc dir (containing the monitor component and firmware folder) at {lbmsocdir}? (ENTER for yes, otherwise input the correct path > ")
    if a != "":
        lbmsocdir = a
    

    # Platform file
    platform = "basys3"
    a = input(colored("[SETUP/PLATOFRM] ", "green") + "What is the name of your platform file (without .py)? (default: basys3, press enter to default) > ")
    if a != "":
        platform = a
    
    a = input(colored("[SETUP/PLATFORM-PATH] ", "green") + "Input the path of your platform file to copy it into this project or press enter to skip > ")
    if a != "":
        import shutil
        print(colored("[SETUP/PLATFORM-PATH] ", "green") + "Trying to copy the platform file...")
        try:
            shutil.copyfile(a, lbmsocdir)
            print(colored("[SETUP/PLATFORM-PATH] ", "green") + "Successfully copied.")
        except Exception:
            print(colored("[SETUP/PLATFORM-PATH] ", "red") + "Something went wrong copying the platform file!")
        

    # Create sourcing file
    env_file_path = os.path.join(lbmsocdir, "..", "scripts", "lbm_env_vars.sh")
    print(colored("[SETUP] ", "green") + f"Creating env vars file at {env_file_path}...")
    with open(env_file_path, 'w+') as f:
        f.write(bashrcsource + "\n")
        f.write(f"PLATFORMFILE=\"{platform}\"\n")
        f.write(f"LITEXDIR=\"{litexdir}\"\n")
        f.write(f"LBMSOCDIR=\"{lbmsocdir}\"\n")
    print(colored("[SETUP] ", "green") + "Making env vars shell script executable...")
    os.system("chmod a+x " + env_file_path)
    print(colored("[SETUP] ", "green") + "Done.")

    return bashrcsource, litexdir, lbmsocdir

def setup(args):
    if args.full:
        print(colored("[SETUP] ", "green") + "Setting up the LiteBusMonitor Build Env")
        bashrcsource, litexdir, lbmsocdir = create_litex_env_vars()
        uartpath = os.path.join(litexdir, "soc", "software", "libbase", "uart.c")
        make_uart_polling(uartpath)
    elif args.uart_polling:
        from litex.soc.integration.builder import soc_directory
        uartpath = os.path.join(soc_directory, "software", "libbase", "uart.c")
        make_uart_polling(uartpath)

parser = argparse.ArgumentParser(prog="LiteBusMonitor Setup")
subparsers = parser.add_subparsers()

# Initial setup (Turn UART polling on, create env vars)
setup_parser = subparsers.add_parser("setup")
group1 = setup_parser.add_mutually_exclusive_group()
group1.add_argument("--full", action="store_true", default=True, help="Complete setup. ")
group1.add_argument("--uart-polling", action="store_true", default=False, help="Set UART to polling in the litex source tree")
setup_parser.set_defaults(func=setup)

# Insert the needed declarations and functions into the litex bios/libbase code dir
bios_inject = subparsers.add_parser("bios")

# Run and capture
# TODO: This or the script task system?
runner = subparsers.add_parser("run")
runner.set_defaults(func=run)

args = parser.parse_args()
args.func(args)



