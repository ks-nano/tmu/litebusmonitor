source lbm_env_vars.sh
shopt -s expand_aliases
cd litebusmonitor
py gen.py $PLATFORMFILE --build --l2-size 0 --integrated-main-ram-size 41384 --integrated-rom-size=64000 --integrated-sram-size=19000 --uart-baudrate 1000000 --doc
# cd firmware
# make
rm $LITEXDIR/soc/cores/cpu/vexriscv/crt0.o
