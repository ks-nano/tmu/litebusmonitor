source lbm_env_vars.sh
shopt -s expand_aliases
if [ "$1" == "boot" ]; then
    litexterm /dev/ttyUSB1 --kernel=$LBMSOCDIR/firmware/firmware.bin --speed 1000000
elif [ "$1" == "noboot" ]; then
    litexterm /dev/ttyUSB1 --speed 1000000
else
    echo "Invalid Mode"
fi
