#!/usr/bin/env python3

from setuptools import setup
from setuptools import find_packages

setup(
    name="litebusmonitor",
    description="bus monitor",
    long_description="",
    author="Christian Klarhorst",
    author_email="cklarhor@techfak.uni-bielefeld.de",
    license="MIT",
    python_requires="~=3.6",
    packages=find_packages(where="src"),
    entry_points={},
    install_requires=['meson==0.60.1', 'ninja==1.10.2.3', 'termcolor', 'bitstring', 'tqdm', 'numpy', 'pandas', 'pyarrow'],
    py_modules=['litebusmonitor/monitor', 'litebusmonitor/receiver']
)
