import random
import unittest
import sys
from tqdm import tqdm
import os
adddir = os.getcwd()
adddir = os.path.join(adddir, "litebusmonitor")
print("Adding " + adddir + " to python PATH")
sys.path.append(adddir)

from litebusmonitor.bus_signal_monitors import BusMasterMonitor


from migen import *
from litex.soc.integration.soc import SoC, SoCBusHandler, SoCRegion
from litex.gen.sim import run_simulation
from litex.soc.interconnect.stream import *
from litex.soc.interconnect import wishbone

QUEUEDEPTH = 20

class DUT(Module):
    def __init__(self, DEPTH) -> None:

        self.bus = bus = SoCBusHandler()
        self.submodules.sbus = bus

        self.m1 = wishbone.Interface()
        self.m2 = wishbone.Interface()

        self.s1 = wishbone.Interface()

        ram = wishbone.SRAM(300, bus=self.s1)
        self.submodules += ram

        sramregion = SoCRegion(0x0, size=300*8)
       
        bus.add_master("m1", self.m1)
        bus.add_master("m2", self.m2)
        bus.add_slave("testram", ram.bus, sramregion)

        
        # WARNING: works only for one master
        # see soc.py:1030
        self.submodules.bus_interconnect = wishbone.Crossbar(
            masters = list(self.bus.masters.values()),
            slaves  = [(self.bus.regions[n].decoder(self.bus), s) for n, s in self.bus.slaves.items()]
        )


        self.monSelect = BusMasterMonitor([self.m1, self.m2], "ack", DEPTH, selectByAck=True)
        self.submodules += self.monSelect

        self.monNormal = BusMasterMonitor([self.m1, self.m2], "ack", DEPTH, selectByAck=False)
        self.submodules += self.monNormal


    def writebus(self, masterName, adr, val):
        yield from self.bus.masters[masterName].write(adr, val)

    def readbus(self, masterName, adr):
        yield from self.bus.masters[masterName].read(adr)


    def usebus(self, mn, ad, v=None):
        if v is None:
            yield from self.readbus(mn, ad)
        else:
            yield from self.writebus(mn, ad, v)

    
    def wait(self, cycles):
        for i in range(cycles):
            yield


class BusMonitorAckTriggeredTest(unittest.TestCase):
    def setUp(self):
        self.dut = DUT(200)


    def test(self):
        def testfunction():
            print("Testing ack activated bus monitor with selectByAck...")
            # Generate random accesses
            accesses = []
            for i in tqdm(range(100)):
                accessdict = {}
                self.dut.wait(random.randint(1,10))
                accessdict["firingMaster"] = random.choice(["m1", "m2"])
                accessdict["adr"] = random.randint(0,150)
                if random.randint(0,1) == 1:
                    # Write
                    accessdict["rw"] = 1
                    yield from self.dut.writebus(accessdict["firingMaster"], accessdict["adr"], random.randint(0,1000))
                else:
                    # Read
                    accessdict["rw"] = 0
                    yield from self.dut.readbus(accessdict["firingMaster"], accessdict["adr"])
                
                yield
                accesses.append(accessdict)

            print("Asserting selectByAck Monitor...")
            # Check queue output
            source = self.dut.monSelect.source
            yield source.ready.eq(1)
            yield
            for i in tqdm(range(len(accesses))):
                while (yield source.valid != 1):
                    yield
                
                adr = (yield source.__getattr__("adr"))
                rw = (yield source.__getattr__("we"))
                fm = (yield source.__getattr__("firingMaster"))

                self.assertEqual(adr, accesses[i]["adr"])
                self.assertEqual(rw, accesses[i]["rw"])
                self.assertEqual({0:"m1", 1:"m2"}[fm], accesses[i]["firingMaster"])
                yield


            print("Asserting normal monitor...")
            source = self.dut.monNormal.source
            yield source.ready.eq(1)
            yield
            for i in tqdm(range(len(accesses))):
                while (yield source.valid != 1):
                    yield
    
                master = str({"m1":0, "m2":1}[accesses[i]["firingMaster"]])
                self.assertEqual((yield source.__getattr__(master + "adr")), accesses[i]["adr"])
                self.assertEqual((yield source.__getattr__(master + "we")), accesses[i]["rw"])
                self.assertEqual((yield source.__getattr__(master + "ack")), 1)
                yield

        run_simulation(self.dut, testfunction(), vcd_name="test_busmonitor.vcd")
