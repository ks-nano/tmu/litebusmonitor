from multiprocessing.connection import wait
import unittest
import sys
import os
adddir = os.getcwd()
adddir = os.path.join(adddir, "litebusmonitor")
print("Adding " + adddir + " to python PATH")
sys.path.append(adddir)

from litebusmonitor.bus_signal_monitors import SignalMonitor

from migen import *
from litex.soc.integration.soc import SoC, SoCBusHandler, SoCRegion
from litex.gen.sim import run_simulation
from typing import List
import random
from litex.soc.interconnect.stream import *
from litex.soc.interconnect import wishbone


class DUT(Module):
    def __init__(self, mode) -> None:
        self.counter = Signal(32)
        self.sync += self.counter.eq(self.counter + 1)
        
        self.trigger = Signal()
        self.value = Signal(32)

        self.monitor = SignalMonitor(self.value if mode == "change" else self.trigger, [self.value], 1000, mode="sync", triggerMode=mode)
        self.submodules += self.monitor

        

def generateSignalWithValues_change(valueSignal, values:List, triggerSignal):
    """Set the given signal to the different values, with a random number of cycles between it (2-10). returns a list of all the waittimes before the new signal arrived"""
    print("Generating signals")
    waittimes = []
    for val in values:
        print(str(val), end=" ")
        waittime = random.randint(2,10)
        waittimes.append(waittime)
        for i in range(waittime):
            yield
        yield valueSignal.eq(val)
        yield triggerSignal.eq(1)
        yield
        yield triggerSignal.eq(0)
    print()
    return waittimes

def generateSignal(triggerList, valueList, dut:DUT, triggerMode="changeRising"):
    """
    This method sets the value signal to the i-th index of valueList and waits a 2-10 cycles. Then it sets the trigger according to its mode and the ith index of
    triggerList. So when triggerList=[1,0,0,1,0] and valueList=[42,5,7,9,1] the values 42 and 9 should be recorded
    """
    print(f"Generating signals for mode {triggerMode}")
    if len(triggerList) != len(valueList):
        print("Mismatch in triggerlist and valuelist length")
        sys.exit()

    for i in range(len(valueList)):
        print(str(i), end=" ")
        # Reset
        if triggerMode == "changeRising":
            yield dut.trigger.eq(0)
        elif triggerMode == "changeFalling":
            yield dut.trigger.eq(1)
        yield

        # Setting value
        yield dut.value.eq(valueList[i])
        yield

        # Waiting
        for j in range(random.randint(2,10)):
            yield
        
        # Set trigger
        if triggerMode == "changeRising" and triggerList[i] == 1:
            yield dut.trigger.eq(1)
        elif triggerMode == "changeFalling" and triggerList[i] == 1:
            yield dut.trigger.eq(0)
        yield
    print()

def wait(t):
    print("Waiting for " + str(t) + " cycles...")
    for i in range(t):
        yield

def assertOutQueue(values, waittimes, testobject:unittest.TestCase):
    print("Asserting values...")
    fifo = testobject.dut.monitor.fifo
    yield fifo.source.ready.eq(1)
    yield
    for val in values:
        print(str(val), end=" ")
        while (yield fifo.source.valid != 1):
            yield
        read = (yield fifo.source.sig0)
        testobject.assertEqual(read, val, f"Value Read {read}, value expected {val}")
        yield
    print()

class MonitorTestChange(unittest.TestCase):
    def setUp(self) -> None:
        self.dut = DUT(mode="change")
        
    def changetest(self):
        yield from generateSignalWithValues_change(self.dut.value, [10, 12, 19, 564, 21], self.dut.trigger)
        yield from wait(100)
        yield from assertOutQueue([10, 12, 19, 564, 21], [], self)

    def test(self):
        def testfunction():
            yield from self.changetest()
        run_simulation(self.dut, testfunction(), vcd_name="test_change_Monitor.vcd")

class MonitorTestRising(unittest.TestCase):
    def setUp(self) -> None:
        self.dut = DUT(mode="changeRising")
    
    def risingtest(self):
        yield from generateSignal([1,0,0,1,0,0,1], [1,2,3,4,5,6,7], self.dut, "changeRising")
        yield from wait(100)
        yield from assertOutQueue([1,4,7], [], self)

    def test(self):
        def testfunction():
            yield from self.risingtest()
        run_simulation(self.dut, testfunction(), vcd_name="test_changeRising_Monitor.vcd")


class MonitorTestFalling(unittest.TestCase):
    def setUp(self) -> None:
        self.dut = DUT(mode="changeFalling")
    
    def fallingtest(self):
        yield from generateSignal([1,1,1,0,0,1,0], [1,2,3,4,5,6,7], self.dut, "changeFalling")
        yield from wait(100)
        yield from assertOutQueue([1,2,3,6], [], self)

    def test(self):
        def testfunction():
            yield from self.fallingtest()
        run_simulation(self.dut, testfunction(), vcd_name="test_changeFalling_Monitor.vcd")

class MonitorTestTrue(unittest.TestCase):
    def setUp(self) -> None:
        self.dut = DUT(mode="true")
    
    def truetest(self):
        print("Generating values...")
        res = []
        for val in [6,3,5,9]:
            print(str(val), end=" ")
            yield self.dut.value.eq(val)
            yield
            yield self.dut.trigger.eq(1)
            yield
            waittime = random.randint(2,10)
            for i in range(waittime):
                yield
            yield self.dut.trigger.eq(0)
            yield
            res += [val for i in range(waittime+1)]
        print()

        if len(res) > 1000:
            print("Too many numbers")
            sys.exit()

        yield from wait(100)
        yield from assertOutQueue(res, [], self)

    def test(self):
        def testfunction():
            yield from self.truetest()
        run_simulation(self.dut, testfunction(), vcd_name="test_true_Monitor.vcd")